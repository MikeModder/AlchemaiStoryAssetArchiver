const args = require('yargs')
    .option('baseurl', {
        describe: 'The URL to use as the base',
        alias: 'u'
    })
    .option('clean', {
        describe: 'Delete existing download folder (if it exists)',
        alias: 'c',
        type: 'boolean'
    })
    .default('baseurl', 'https://alchemiastory-jp.sslcs.cdngc.net/alchemiastory/release_20180613_33/android/assets/5_3/', 'Android, 13/06/18')
    .default('clean', false, 'No')
    .help()
    .argv;

const request = require('request');
const progress = require('request-progress');
const fs = require('fs');
const ora = require('ora');
const KStream = require('kaitai-struct/KaitaiStream');
const Msf = require('./Mfs');
const fse = require('fs-extra');
const path = require('path');

const baseUrl = args.baseurl;

if(args.clean) fse.removeSync('./download');

main();

async function main(){
    await downloadFile('manifest/root.mfs');
    let root = new Msf(new KStream(fs.readFileSync('./download/manifest/root.mfs')));
    let i = 0;
    await eachOf(root.files, async (f, _a, _b) => {
        i++
        await downloadFile(f.name, i, root.files.length);
    });
    let manifests = fs.readdirSync('./download/manifest');
    i = 0;
    await eachOf(manifests, async (m, _a, _b) => {
        let man = new Msf(new KStream(fs.readFileSync('./download/manifest/'+m)))
        await eachOf(man.files, async (f, _c, _d) => {
            i++;
            await downloadFile(f.name, i, man.files.length);
        });
    });
}

async function eachOf(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }

function downloadFile(filename, current, total) {
    return new Promise(async (resolve, reject) => {
        await fse.ensureDir(path.dirname('./download/'+filename))
        const url = baseUrl + filename;
        const dlSpinner = ora(`Downloading ${filename}... 0% [${current} of ${total}]`).start();
        progress(request(url))
            .on('progress', (status) => {
                dlSpinner.text = `Downloading ${filename}... ${Math.round(status.percent * 100)}% [${current} of ${total}]`;
            })
            .on('error', (err) => {
                dlSpinner.fail(err);
                reject(err);
            })
            .on('end', () => {
                dlSpinner.succeed(`Downloaded ${filename} successfully!`);
                resolve(null);
            })
            .pipe(fse.createWriteStream('./download/'+filename));
    });
}
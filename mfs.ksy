meta:
  id: mfs
  file-extension: mfs
  endian: le
seq:
  - id: magic
    contents: gmfs
  - id: version
    size: 4
  - id: filecount
    size: 4
  - id: files
    type: filename
    repeat: eos

types:
  filename:
    seq:
      - id: len
        type: u1
      - id: name
        type: str
        encoding: UTF-8
        size: len
      - id: unk0
        size: 16
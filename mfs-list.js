#!/usr/bin/env node
const args = require('yargs')
    .option('file', {
        alias: 'f',
        describe: 'Input .mfs file'
    })
    .demand(['file'])
    .help()
    .argv;

const fs = require('fs');
const Msf = require('./Mfs');
const KStream = require('kaitai-struct/KaitaiStream');

console.log('msf-list by MikeModder, with help from Kaitai');

const inputFileExists = fs.existsSync(args.file);
if(!inputFileExists) { console.error(`Input file "${args.file}" does not exist!`); process.exit(3); }

const file = new Msf(new KStream(fs.readFileSync(args.file)));
console.log(`[File name] (${file.filecount} files)`)
file.files.forEach(f => {
    console.log(`[${f.name}]`);
});
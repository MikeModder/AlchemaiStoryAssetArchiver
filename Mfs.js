// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Mfs = factory(root.KaitaiStream);
  }
}(this, function (KaitaiStream) {
var Mfs = (function() {
  function Mfs(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Mfs.prototype._read = function() {
    this.magic = this._io.ensureFixedContents([103, 109, 102, 115]);
    this.version = this._io.readBytes(4);
    this.filecount = this._io.readBytes(4);
    this.files = [];
    var i = 0;
    while (!this._io.isEof()) {
      this.files.push(new Filename(this._io, this, this._root));
      i++;
    }
  }

  var Filename = Mfs.Filename = (function() {
    function Filename(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Filename.prototype._read = function() {
      this.len = this._io.readU1();
      this.name = KaitaiStream.bytesToStr(this._io.readBytes(this.len), "UTF-8");
      this.unk0 = this._io.readBytes(16);
    }

    return Filename;
  })();

  return Mfs;
})();
return Mfs;
}));
